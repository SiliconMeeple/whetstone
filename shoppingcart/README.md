Shopping cart
=============

Having a play with a toy problem used for interviewing.

The problem
===========

1. You are building a checkout system for a shop which only sells apples and oranges.
 * Apples cost 60p and oranges 25p.
 * Build a checkout system which takes a list of items scanned at the till and outputs the total cost.
 * For example: [Apple, Apple, Orange, Apple] => £2.05

2. Simple offers
 * The shop decides to introduce two new offers
   * Buy one get one free on Apples
   * 3 for the price of 2 on oranges

3. More complicated offers
  * Bananas cost 20p
  * Bananas are added to teh same bogof offer as apples, giving the cheapest item free.

4.
  * Melons cost £1
  * Melons are available through a seperate 3 for 2 offer.

5. Realtime checkout
  * Show a running total as items are scanned.

Stretch.
  * Customers could save money by making multiple trips.
  * Fix the problem.
