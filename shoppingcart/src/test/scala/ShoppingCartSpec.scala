import org.scalatest.{ShouldMatchers, WordSpec}
import store._

import scala.collection.immutable.Map

object Helpers {
  implicit class StringToSeq(n: Int) {
    def *(str: String) = Seq.fill(n)(str)
  }
}

import Helpers._

class ShoppingCartSpec extends WordSpec with ShouldMatchers {

  val Item = "item"
  val Item2 = "item2"

  "A shopping cart" should {

    val priceCalculator = (xs:QuantityMap) => xs.head._2

    val costOf = store.costOf(Map(Item -> priceCalculator)) _

    "complain if unexpected items are found" in {
      val thrown = the[IllegalArgumentException] thrownBy costOf(Seq("ThirtyTwoInchTv", "kiwifruit"))
      thrown should have message "Unexpected items in the bagging area: ThirtyTwoInchTv, kiwifruit"
    }

    "apply a price calculator to its item" in {
      costOf(Seq(Item)) shouldBe 1
    }

    "correctly count two items" in {
      costOf(2 * Item) shouldBe 2
    }

    "correctly apply multiple price calculators" in {
      val itemCount = (xs: QuantityMap) => {xs shouldBe Map(Item -> 2); 1 }
      val item2Count = (xs: QuantityMap) => {xs shouldBe Map(Item2 -> 3); 2 }

      store.costOf(Map(Item -> itemCount, Item2 -> item2Count))(2 * Item ++ 3 * Item2) shouldBe 3
    }

    "correctly apply the same price calculator to multiple items" in {
      val itemCount = (xs: QuantityMap) => {xs shouldBe Map(Item -> 2, Item2 -> 3); 1}
      store.costOf(Map(Item -> itemCount, Item2 -> itemCount))(2 * Item ++ 3 * Item2) shouldBe 1
    }
  }

  "bogof" should {

    "give the second item free" in {
      bogof(Map(Item -> 2)) shouldBe Map(Item -> 1)
    }

    "Charge for a third item" in {
      bogof(Map(Item -> 3)) shouldBe Map(Item -> 2)
    }

    "Charge for many items" in {
      bogof(Map(Item -> 11)) shouldBe Map(Item -> 6)
    }

    "Apply the discount over multiple items in order" in {
      bogof(Map(Item -> 1, Item2 -> 1)) shouldBe Map(Item -> 0, Item2 -> 1)
    }
  }

  "b3g1f" should {
    "give the 3rd item free" in {
      b3g2f(Map(Item -> 3)) shouldBe Map(Item -> 2)
    }

    "charge for a 4th and 5th items" in {
      b3g2f(Map(Item -> 5)) shouldBe Map(Item -> 4)
    }

    "apply the offer to many items" in {
      b3g2f(Map(Item -> 11)) shouldBe Map(Item -> 8)
    }

    "apply the discount to multiple items in order" in {
      b3g2f(Map(Item -> 2, Item2 -> 1)) shouldBe Map(Item -> 1, Item2 -> 1)
    }

  }
}
