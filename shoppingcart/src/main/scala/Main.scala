import store._

object Main extends App {
  val appleAndBananaOffer = bogof.andThen(cost("apple" -> 60, "banana" -> 20))
  val shoppingCost = costOf(Map("apple" -> appleAndBananaOffer,
                                "orange" -> b3g2f.andThen(cost("orange" -> 25)),
                                "banana" -> appleAndBananaOffer,
                                "melon" -> b3g2f.andThen(cost("melon" -> 100)))) _

  println(s"Your shopping cart contains: ${args.mkString("[",",","]")}, for a cost of: ${shoppingCost(args.toSeq.map(_.toLowerCase))}")
}
