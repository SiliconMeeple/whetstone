import scala.collection.immutable.ListMap
import scala.collection.breakOut

package object store {

  type Item = String
  type Price = Int
  type QuantityMap = Map[Item, Int]
  type PriceCalculator = QuantityMap => Price

  private def total(qties: QuantityMap) = qties.map(_._2).sum

  def cost(prices: (Item, Price)*): PriceCalculator = (quantities: QuantityMap) => {
    val priceMap = prices.toMap
    (for (item <- quantities.keys) yield priceMap(item) * quantities(item)).sum
  }

  def discount(qties: QuantityMap, numToDiscount: Int): QuantityMap = {
    var quantityToDiscount = numToDiscount
    qties.map{ case (item, qty) =>
      val oldDiscount = quantityToDiscount
      quantityToDiscount = Math.max(quantityToDiscount - qty, 0)
      (item, Math.max(qty - oldDiscount, 0))
    }
  }

  val bogof = (qties: QuantityMap) => discount(qties, total(qties) / 2)

  val b3g2f = (qties: QuantityMap) => discount(qties, total(qties) / 3)

  def costOf(itemsInStore: Map[Item, PriceCalculator])(items: Seq[Item]): Price = {
    val unknownItems = items.filter(item => !itemsInStore.keySet.contains(item))

    if (unknownItems.nonEmpty) {
      throw new IllegalArgumentException(s"Unexpected items in the bagging area: ${unknownItems.mkString(", ")}")
    }

    items.groupBy(item => itemsInStore(item)).mapValues(_.groupBy(item => item).mapValues(_.length)).map {
      case (priceCalculator, itemAndQuantity) => priceCalculator(itemAndQuantity)
    }.sum
  }
}
