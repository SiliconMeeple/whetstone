import mill._, scalalib._

object foo extends ScalaModule {
    def scalaVersion = "2.12.8"

    def scalacOptions = Seq("-Ydelambdafy:inline")

    def ivyDeps = Agg(
        ivy"com.lihaoyi::upickle:0.5.1",
        ivy"com.lihaoyi::pprint:0.5.2",
        ivy"com.lihaoyi::fansi:0.2.4"
    )

    object test extends Tests{
        def ivyDeps = Agg(ivy"org.scalatest::scalatest:3.0.4")
        def testFrameworks = Seq("org.scalatest.tools.Framework")
    }


    def compile = T {
        println("Compiling...")
        val result = super.compile()
        println("Compiled.")
        result
    }


    def bar = T { "hello" }
    object baz extends mill.Module {
        def qux = T { "world" }
    }
}
