(load-file "util.clj")

(def __ (fn [primeCount] (loop [res [2] x 3]
                           (if (= (count res) primeCount) res
                               
                               (if (some #(= 0 (mod x %)) res)
                                 (recur res (inc x))
                                 (recur (conj res x) (inc x)))))))

(print-tests (= (__ 2) [2 3])
             
             (= (__ 5) [2 3 5 7 11])
             
             (= (last (__ 100)) 541))