(load-file "util.clj")

(def __ (fn [p coll] (reduce (fn [ret x] (let [k (p x)] (assoc ret k (conj (get ret k []) x)))) {} coll)))

(print-tests (= (__ #(> % 5) [1 3 6 8]) {false [1 3], true [6 8]})
             
             (= (__ #(apply / %) [[1 2] [2 4] [4 6] [3 6]])
                {1/2 [[1 2] [2 4] [3 6]], 2/3 [[4 6]]})
             
             (= (__ count [[1] [1 2] [3] [1 2 3] [2 3]])
                {1 [[1] [3]], 2 [[1 2] [2 3]], 3 [[1 2 3]]})
)