(use 'clojure.test)
(load-file "util.clj")

(def __ (fn [relation] (loop [rel  (into {} (for [[k v] relation] [k #{v}]))]
                         (let [newrel (reduce (fn [r x] (assoc r x (apply clojure.set/union (conj (for [val (r x)] (r val)) (r x))))) rel (keys rel))
                               toSet (fn [clos] (into #{} (mapcat (fn [[k s]] (map #(vector k %) s)) clos)))]
                           (if (= rel newrel) (toSet rel) (recur newrel))
                           ))))

(deftest tests (are [x] x
                    
                    (let [divides #{[8 4] [9 3] [4 2] [27 9]}]
                      (= (__ divides) #{[4 2] [8 4] [8 2] [9 3] [27 9] [27 3]}))
                    
                    (let [more-legs
                          #{["cat" "man"] ["man" "snake"] ["spider" "cat"]}]
                      (= (__ more-legs)
                         #{["cat" "man"] ["cat" "snake"] ["man" "snake"]
                           ["spider" "cat"] ["spider" "man"] ["spider" "snake"]}))
                    
                    (let [progeny
                          #{["father" "son"] ["uncle" "cousin"] ["son" "grandson"]}]
                      (= (__ progeny)
                         #{["father" "son"] ["father" "grandson"]
                           ["uncle" "cousin"] ["son" "grandson"]}))
                    ))
(run-tests)