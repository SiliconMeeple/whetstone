(load-file "util.clj")

(def __ (fn reductions2
          ([f coll] (reductions2 f (first coll) (rest coll)))
          ([f init [x & xs]] (cons init (lazy-seq (when x (reductions2 f (f init x) xs)))) )))


(print-tests 	
 (= (take 5 (__ + (range))) [0 1 3 6 10])
 
 (= (__ conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])
 
 (= (last (__ * 2 [3 4 5])) (reduce * 2 [3 4 5]) 120))