(load-file "util.clj")

(def __ (fn [b] (let [transpose #(apply map vector %)
                      players   #{:x :o}
                      all-same #(reduce (fn [a x] (if (= a x) a nil)) %)
                      find-winner (fn [lines] (some #(players (all-same %)) lines))
                      diag #(vector (map nth % (range 3)) (map nth % (range 2 -1 -1)))]
                  (find-winner (concat b (transpose b) (diag b)))
                  )))

(print-tests (= nil (__ [[:e :e :e]
                         [:e :e :e]
                         [:e :e :e]]))
             
             (= :x (__ [[:x :e :o]
                        [:x :e :e]
                        [:x :e :o]]))
             
             (= :o (__ [[:e :x :e]
                        [:o :o :o]
                        [:x :e :x]]))
             
             (= nil (__ [[:x :e :o]
                         [:x :x :e]
                         [:o :x :o]]))
             
             (= :x (__ [[:x :e :e]
                        [:o :x :e]
                        [:o :e :x]]))
             
             (= :o (__ [[:x :e :o]
                        [:x :o :e]
                        [:o :e :x]]))
             
             (= nil (__ [[:x :o :x]
                         [:x :o :x]
                         [:o :x :o]])))