(load-file "util.clj")

(def __ (fn [coll]  (->> coll
                         (group-by frequencies)
                         vals
                         (filter #(> (count %) 1))
                         (map set)
                         set)))

(print-tests  (= (__ ["meat" "mat" "team" "mate" "eat"])
                 #{#{"meat" "team" "mate"}})
              
              (= (__ ["veer" "lake" "item" "kale" "mite" "ever"])
                 #{#{"veer" "ever"} #{"lake" "kale"} #{"mite" "item"}}))
