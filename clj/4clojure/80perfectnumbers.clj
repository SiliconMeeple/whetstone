(use 'clojure.test)
(load-file "util.clj")

(def __ (fn [n] (let [divisors (fn [n] (filter #(= 0 (mod n %)) (range 1 (inc (int (/ n 2))))))]
                  (= n (apply + ( divisors n))))))

(deftest tests
  (is	
   (= (__ 6) true))
  (is
   (= (__ 7) false))
  (is	
   (= (__ 496) true))
  (is	
   (= (__ 500) false))
  (is	
   (= (__ 8128) true)))

(run-tests)