(defn trace [x] (do (println x) x))


(def __  (fn [l u] (take (- u l) (iterate inc l))))


(apply println (list (= (__ 1 4) '(1 2 3))
                     
                     (= (__ -2 2) '(-2 -1 0 1))
                     
                     (= (__ 5 8) '(5 6 7))))