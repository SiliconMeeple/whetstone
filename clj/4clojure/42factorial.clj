(load-file "util.clj")

(def __ (fn [x] (reduce #(* %1 %2) (range 1 (inc x)))))

(print-tests (= (__ 1) 1)
             
             (= (__ 3) 6)
             
             (= (__ 5) 120)
             
             (= (__ 8) 40320))