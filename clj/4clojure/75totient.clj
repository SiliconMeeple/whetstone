(load-file "util.clj")

(def __ (fn [x] (let [gcd (fn gcd [a0 b0] (loop [a a0 b b0] (if (= b 0) a (recur b (mod a b)))))]
                  (->> x
                       (range 2)
                       (filter #(= 1 (gcd x %)))
                       count
                       inc
                       ))))

(print-tests 	
 (= (__ 1) 1)
 
 (= (__ 10) (count '(1 3 7 9)) 4)
 
 (= (__ 40) 16)
 
 (= (__ 99) 60))