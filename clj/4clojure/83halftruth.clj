(use 'clojure.test)


(def __ (fn [& bs] (= #{true false} (into #{} bs))))

(deftest tests (are [x] x	
                    (= false (__ false false))
                    
                    (= true (__ true false))
                    
                    (= false (__ true))
                    
                    (= true (__ false true false))
                    
                    (= false (__ true true true))
                    
                    (= true (__ true true true false))))

(run-tests)