(load-file "util.clj")

(def __ (fn [xs]
          (let [r
                (loop [[head & tail] (rest xs)
                       streak [(first xs)]
                       output []]
                  (cond
                   (nil? head) (if (> (count streak) (count output)) streak output)
                   (> head (peek streak)) (recur tail (conj streak head) output)
                   true (if (> (count streak) (count output))
                          (recur tail [head] streak)
                          (recur tail [head] output))))]
            (if (> (count r) 1) r []))))

(print-tests	
 (= (__ [1 0 1 2 3 0 4 5]) [0 1 2 3])
 
 (= (__ [5 6 1 3 2 7]) [5 6])
 
 (= (__ [2 3 3 4 5]) [3 4 5])
 
 (= (__ [7 6 5 4]) []))