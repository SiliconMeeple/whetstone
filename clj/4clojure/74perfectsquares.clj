(load-file "util.clj")

(def __ (fn [s] (trace (->> s
                            (re-seq #"\d+")
                            (map #(Integer/parseInt %))
                            (filter (fn [x] (let [r (int (Math/sqrt x))] (= x (* r r))) ) )
                            (clojure.string/join ",")
                            ))))

(print-tests (= (__ "4,5,6,7,8,9") "4,9")
             
             (= (__ "15,16,25,36,37") "16,25,36")
             )