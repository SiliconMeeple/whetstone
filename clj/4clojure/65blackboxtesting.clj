(load-file "util.clj")

(def __ (fn [box]
          (let [x (rand-int 100)
                y (rand-int 100)
                p [x y]
                c (conj box p)]
            (cond
             (= y (get c x)) :map
             (= p (get c p)) :set
             (= x (last (conj c x))) :vector
             :else :list))))

(print-tests (= :map (__ {:a 1, :b 2}))
             
             (= :list (__ (range (rand-int 20))))
             
             (= :vector (__ [1 2 3 4 5 6]))
             
             (= :set (__ #{10 (rand-int 5)}))
             
             (= [:map :set :vector :list] (map __ [{} #{} [] ()])))