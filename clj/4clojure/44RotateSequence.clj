(load-file "util.clj")

(def __ (fn [r xs] (let [s (mod r (count xs))] (concat (drop s xs) (take s xs)))))

(print-tests 	
 (= (__ 2 [1 2 3 4 5]) '(3 4 5 1 2))
 
 (= (__ -2 [1 2 3 4 5]) '(4 5 1 2 3))
 
 (= (__ 6 [1 2 3 4 5]) '(2 3 4 5 1))
 
 (= (__ 1 '(:a :b :c)) '(:b :c :a))
 
 (= (__ -4 '(:a :b :c)) '(:c :a :b)))