
(defn print-tests [& ts] (apply println ts))

(defn trace [x] (do (println x) x))

(defn print-classpath [] (println (seq (.getURLs (java.lang.ClassLoader/getSystemClassLoader)))))