(load-file "util.clj")

(def __ (fn [f & ms]
          (reduce (fn [res m]
                    (reduce (fn [a k] (if (a k)
                                        (assoc a k (f (a k) (m k)))
                                        (assoc a k (m k))))
                            res (keys m)))
                  ms)))

(print-tests 	
 (= (__ * {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})
    {:a 4, :b 6, :c 20})
 
 (= (__ - {1 10, 2 20} {1 3, 2 10, 3 15})
    {1 7, 2 10, 3 15})
 
 (= (__ concat {:a [3], :b [6]} {:a [4 5], :c [8 9]} {:b [7]})
    {:a [3 4 5], :b [6 7], :c [8 9]}) 
)