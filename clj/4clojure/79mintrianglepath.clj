(use 'clojure.test)

(def __ (fn [triangle]
          (let [pad (fn  [xs] (flatten [(first xs) xs (last xs)]))
                minpath (fn [l a b] (+ l (min a b)))]
            (loop [lastLine (first triangle)
                   rst     (rest triangle)]
              (let [paddedLine (pad lastLine)]
                (if (empty? rst) (apply min lastLine)
                    (recur (map minpath (first rst) paddedLine (rest paddedLine)) (rest rst))))))))

(deftest tests 
  (is 	
   (= 7 (__ '([1]
                [2 4]
                  [5 1 4]
                    [2 3 4 5])))) ; 1->2->1->3

  (is
   (= 20 (__ '([3]
                 [2 4]
                   [1 9 3]
                     [9 9 2 4]
                       [4 6 6 7 8]
                         [5 7 3 5 1 4]))))) ; 3->4->3->2->7->1
(run-tests)