(use 'clojure.test)

(def __ (fn [a b] (into #{} (filter a b))))

(deftest tests
  (is (= (__ #{0 1 2 3} #{2 3 4 5}) #{2 3}))
  
  (is   (= (__ #{0 1 2} #{3 4 5}) #{}))

  (is  (= (__ #{:a :b :c :d} #{:c :e :a :f :d}) #{:a :c :d})))

(run-tests)