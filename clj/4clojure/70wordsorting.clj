(load-file "util.clj")

(def __ (fn [s] (trace (sort-by #(.toLowerCase %) (re-seq #"\w+" s)))))

(print-tests (= (__  "Have a nice day.")
                ["a" "day" "Have" "nice"])
             
             (= (__  "Clojure is a fun language!")
                ["a" "Clojure" "fun" "is" "language"])
             
             (= (__  "Fools fall for foolish follies.")
                ["fall" "follies" "foolish" "Fools" "for"]))