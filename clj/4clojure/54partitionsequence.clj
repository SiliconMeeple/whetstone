(load-file "util.clj")

(def __ (fn [n coll] (loop [[head & tail] coll
                            streak []
                            output []]
                       (let [newstreak (conj streak head)]
                         (cond
                          (nil? head) output
                          (= n (count newstreak)) (recur tail [] (conj output newstreak))
                          true (recur tail newstreak output))))))

(print-tests (= (__ 3 (range 9)) '((0 1 2) (3 4 5) (6 7 8)))
             
             (= (__ 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))
             
             (= (__ 3 (range 8)) '((0 1 2) (3 4 5))))