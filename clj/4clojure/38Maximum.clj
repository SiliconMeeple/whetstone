(defn trace [x] (do (println x) x))

(def __ (fn [& xs] (reduce #(if (> %1 %2) %1 %2) xs)))

(apply println (list (= (__ 1 8 3 4) 8)
                     
                     (= (__ 30 20) 30)
                     
                     (= (__ 45 67 11) 67)))