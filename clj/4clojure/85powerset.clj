(use 'clojure.test)

(def __ (fn [coll] (reduce (fn [s e] (set (concat s [#{e}] (map #(conj % e) s)))) #{#{}} coll)))

(deftest tests (are [x] x
                    (= (__ #{1 :a}) #{#{1 :a} #{:a} #{} #{1}})
                    
                    (= (__ #{}) #{#{}})
                    
                    (= (__ #{1 2 3})
                       #{#{} #{1} #{2} #{3} #{1 2} #{1 3} #{2 3} #{1 2 3}})
                    
                    (= (count (__ (into #{} (range 10)))) 1024)

                    ))

(run-tests)