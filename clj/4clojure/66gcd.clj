(load-file "util.clj")

(def __ (fn gcd [a0 b0] (loop [a a0 b b0] (if (= b 0) a (recur b (mod a b))))))

(print-tests (= (__ 2 4) 2)
             
             (= (__ 10 5) 5)
             
             (= (__ 5 7) 1)
             
             (= (__ 1023 858) 33))