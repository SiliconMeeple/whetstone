(use 'clojure.test)
(load-file "util.clj")

(def __ (fn [start] (letfn [ (digits [n]  (map #(- (int %) (int \0)) (str n)))
                         (sum-of-squares [coll] (reduce + (map #(* % %) coll)))]
                      (= 1 (some #{1 4} (iterate (comp sum-of-squares digits) start))))))

(deftest tests (are [x] x
                    (= (__ 7) true)
                    
                    (= (__ 986543210) true)
                    
                    (= (__ 2) false)
                    
                    (= (__ 3) false)
                    ))

(run-tests)