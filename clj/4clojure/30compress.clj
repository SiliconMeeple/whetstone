
(def __ (fn [s]
          (reverse  (reduce (fn [a i]
                                      (if (= i (first a))
                                        a
                                        (cons i a)))
                                    [] s))))




(println  (= (apply str (__ "Leeeeeerrroyyy")) "Leroy"))

(println  (= (__ [1 1 2 3 3 2 2 3]) '(1 2 3 2 3)))

(println  (= (__ [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2])))