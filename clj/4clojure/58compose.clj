(load-file "util.clj")

(def __ (fn [& fs]
          (fn [& args]
            (first ( reduce (fn [a f] [(apply f a)]) args (reverse fs))))))

(print-tests (= [3 2 1] ((__ rest reverse) [1 2 3 4]))
             
             (= 5 ((__ (partial + 3) second) [1 2 3 4]))
             
             (= true ((__ zero? #(mod % 8) +) 3 5 7 9))
             
             (= "HELLO" ((__ #(.toUpperCase %) #(apply str %) take) 5 "hello world")))