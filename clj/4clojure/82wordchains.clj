(use 'clojure.test)

(def __ (fn [words] (letfn [(edit-dist [s t]
                              (cond (empty? s) (count t)
                                    (empty? t) (count s)
                                    :else (let [rs (rest s) rt (rest t)]
                                            (if (= (first s) (first t)) (edit-dist rs rt)
                                                (inc (min  (edit-dist rs t)
                                                           (edit-dist s rt)
                                                           (edit-dist rs rt)))))))
                            (find-paths [graph start seen]
                              (if (seen start) seen
                                  (for [n (graph start)]
                                    (find-paths graph n (conj seen start)))))]
                      (let [graph (into {}
                                        (for [word words]
                                          [word (filter #(= 1 (edit-dist % word)) words)]))]
                        (if (some (fn [w]
                                    (some #(= words %)
                                          (flatten (find-paths graph w #{}))))
                                  words)
                          true false)))))

(deftest tests
  (is (= true (__ #{"hat" "coat" "dog" "cat" "oat" "cot" "hot" "hog"})))
  
  (is (= false (__ #{"cot" "hot" "bat" "fat"})))

  (is (= false (__ #{"to" "top" "stop" "tops" "toss"})))

  (is (= true (__ #{"spout" "do" "pot" "pout" "spot" "dot"})))

  (is (= true (__ #{"share" "hares" "shares" "hare" "are"})))

  (is (= false (__ #{"share" "hares" "hare" "are"})))
  )

(run-tests)