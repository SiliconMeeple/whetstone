# Given a Numeric, provide a String representation with commas inserted between 
# each set of three digits in front of the decimal. For example, 1999995.99 should 
# become "1,999,995.99".
quiz = 123456789.123456

puts quiz.to_s.reverse.scan(/(\d*\.\d{3}|\d{3})/).join(",").reverse

#Given a nested Array of Arrays, perform a flatten()-like operation that removes 
#only the top level of nesting. For example, [1, [2, [3]]] would become [1, 2, [3]].
quiz = [1,2,[3,4,[5,6]]]

#Something like the following. Not sure atm though.
quiz.map {|x| if x.class==Array; x.each {|x| x} else x end }

#Shuffle the contents of a provided Array.
quiz = [1,2,3,4,5,6,7,8,9]
puts quiz.sort_by {rand}

# Given a Ruby class name in String form (like "GhostWheel::Expression::LookAhead"),
# fetch the actual class object.
quiz = "Date"
puts eval(quiz + ".new.class")
# Not sure about that one. Seems to work though.

#Insert newlines into a paragraph of prose (provided in a String) so lines will 
# wrap at 40 characters.
quiz=<<END_HERE
This week's Ruby Quiz is in pop quiz format. For each of the scenarios below, send in a one line (80 characters or less) solution that performs the task. You may use any legal Ruby syntax including require statements and semicolons, but the goal in finesse more than golfing.

Any input described in the problem is in a local variable called quiz. Assigning to this variable is assumed to have happened earlier in the program and is not part of your submitted solution.

Your line just needs to evaluate to the expected result. You do not have to store the result in a variable or create any output.

Any edge cases not covered by the provided examples are left to your best judgement.
END_HERE

puts quiz.scan(/([^\n]{40}|[^\n]*\n*[^\n]{0,40})/).join("\n")

# Given an Array of String words, build an Array of only those words that are 
# anagrams of the first word in the Array.

quiz=["one","neo","two","three","eno","four"]
puts quiz.dup.delete_if{ |x| not x.downcase.split('').sort == quiz[0].downcase.split('').sort }

# Convert a ThinkGeek t-shirt slogan (in String form) into a binary 
# representation (still a String).
quiz="you are dumb"
puts (quiz.split(' ').map { |x| (x.split('').map { |y| sprintf("%07b",y[0])}).join}).join("\n")

# Given a wondrous number Integer, produce the sequence (in an Array). A wondrous 
# number is a number that eventually reaches one, if you apply the following rules 
# to build a sequence from it. If the current number in the sequence is even, the 
# next number is that number divided by two. When the current number is odd, 
# multiply that number by three and add one to get the next number in the sequence. 
# Therefore, if we start with the wondrous number 15, the sequence is 
# [15, 46, 23, 70, 35, 106, 53, 160, 80, 40, 20, 10, 5, 16, 8, 4, 2, 1].
quiz = 15
