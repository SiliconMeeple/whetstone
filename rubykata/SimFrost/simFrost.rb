require 'rubygems'
require 'RMagick'
require 'enumerator'
include Magick

 class Array
   def count
    k=Hash.new(0)
    self.flatten.each{|x| k[x]+=1 }
    k
   end
end

class Grid
    
    def initialize(width, height, vapour)
        raise "Each dimension must be even." if ((width % 2 == 1) or (height % 2 == 1))
        @ticks = 0
        @grid = []
        (Array.new(width*height*vapour, :vapour) + Array.new(width*height*(1-vapour), :empty)).sort_by {rand }.each_slice(width) { |s| @grid << s }
        p "#{width/2}, #{height/2}, #{@grid.first.length}, #{@grid.length}"
        @grid[height/2][width/2] = :ice
    end
    
    attr_reader :ticks
    
    def width
        @grid.first.size
    end
    
    def height
        @grid.size
    end
    
    def unfinished?
        @grid.flatten.include?(:vapour)
    end
    
    def vapourLeft?
        @grid.count[:vapour]
    end
    
    def to_s
        @grid.map { |row| row.map { |s|
            @@string_map[s]
        }.join }.join("\n")
    end
    
    def to_image
        image = Image.new(width, height)
        canvas = Draw.new
        canvas.fill_opacity(1)
        height.times do |y|
            width.times do |x|
                if (get([y,x])==:ice)
                    canvas.fill('white')
                elsif (get([y,x])==:vapour)
                    canvas.fill('darkblue')
                else
                    canvas.fill('black')
                end
                canvas.point(x,y)
            end
        end
        canvas.draw(image)
        image
    end
    
    def tick
        ((@ticks % 2)...height).step(2) do |y|
            ((@ticks % 2)...width).step(2) do |x|
                cells = [[y, x],  [y, (x+1) % width],[(y+1) % height, (x+1) % width], [(y + 1) % height, x] ]
                if cells.map { |coord| get(coord) }.member? :ice
                    cells.each { |coord| set(coord,:ice) if get(coord)==:vapour }
                else
                    rotated_cells = cells.dup
                    new_cells = (rand < 0.5 ? rotated_cells.push(rotated_cells.shift) : rotated_cells.unshift(rotated_cells.pop)).map {|coord| get(coord) }
                    cells.zip(new_cells) {|coord, new_value| set(coord, new_value) }
                end
            end
        end
        @ticks+=1
    end
    
    private
    
    @@string_map = { :empty => " ", :vapour => ".", :ice => "*"}
    
    def get(coord)
        @grid[coord.first][coord.last]
    end
    
    def set(coord, value)
        @grid[coord.first][coord.last]=value
    end
end

list = ImageList.new
g = Grid.new(44,160,0.2)
while g.unfinished?
    g.tick
    puts g.vapourLeft?
    $stdout.flush
    list << g.to_image
end

list.write("animation.gif")