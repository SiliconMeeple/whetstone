import cats.data.{IndexedState, State}
import cats.implicits._


final case class Seed(long: Long) {
  def next = Seed(long * 6364136223846793005L + 1442695040888963407L)
}

val nextLong: State[Seed, Long] = State(state =>
  (state.next, state.long))

val nextBoolean: State[Seed, Boolean] = nextLong.map(long =>
  long > 0)


final case class Robot(
                        id: Long,
                        sentient: Boolean,
                        name: String,
                        model: String)

val initialSeed = Seed(13L)

val createRobot: State[Seed, Robot] =
  for {
    id <- nextLong
    sentient <- nextBoolean
    isCatherine <- nextBoolean
    name = if (isCatherine) "Catherine" else "Carlos"
    isReplicant <- nextBoolean
    model = if (isReplicant) "replicant" else "borg"
  } yield Robot(id, sentient, name, model)



val (finalState, robot) = createRobot.run(initialSeed).value
println((finalState, robot))


sealed trait DoorState
case object Open extends DoorState
case object Closed extends DoorState

case class Door(state: DoorState)

// Switching between state types:

import cats.data.IndexedStateT

def open: IndexedState[Closed.type, Open.type, Unit] = IndexedStateT.set(Open)
def close: IndexedState[Open.type, Closed.type, Unit] = IndexedStateT.set(Closed)

//val invalid = for {
//  _ <- open
//  _ <- close
//  _ <- close          // Fails to compile
//} yield ()


val list = List(Some(1), Some(2), None)
// list: List[Option[Int]] = List(Some(1), Some(2), None)

val traversed = list.traverse(identity)

// Looping over states:

case class Position(x : Int = 0, y : Int = 0)

sealed trait Move  extends Product with Serializable
case object Up extends Move
case object Down extends Move
case object Left extends Move
case object Right extends Move

def whereAmI : State[Position, String] = State.inspect{ s => s.toString }

def move(m : Move) : State[Position, String] = State { s =>
  m match {
    case Up => (s.copy(y = s.y + 1), "Up!")
    case Down => (s.copy(y = s.y - 1), "Down!")
    case Left => (s.copy(x = s.x - 1), "Left!")
    case Right => (s.copy(x = s.x + 1), "Right!")
  }
}

val positions : State[Position, List[String]] = for{
  pos1 <- whereAmI
  _ <- move(Up)
  _ <- move(Right)
  _ <- move(Up)
  pos2 <- whereAmI
  _ <- move(Left)
  _ <- move(Left)
  pos3 <- whereAmI
} yield List(pos1,pos2,pos3)


positions.runA(Position()).value

type StatePosition[A] = State[Position, A]

val moves = List(Down, Down, Left, Up)
val result : StatePosition[List[String]] = moves.traverse[StatePosition, String](move)
result.run(Position()).value // (Position(-1,-1),List(Down!, Down!, Left!, Up!))
