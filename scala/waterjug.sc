
// Adapting https://functional.works-hub.com/learn/water-jug-rewrite-with-haskell-part-i-4347a?utm_campaign=p.forteath-fweunewsletter&utm_source=hs_email&utm_medium=email&utm_content=66818119&_hsenc=p2ANqtz-9FkEBH1FHC4Tx3RKJZsZxV0YO4EABVAcPrXwFZ1EZ2EwKLIc4B1tRgbCvgzalo24nioJH7oRchKUCrr4uR-JcuTmxt1w&_hsmi=66818119 to scala

case class Jug(capacity: Int, holding: Int)

implicit val jugOrder: Ordering[Jug] = Ordering.by(Jug.unapply)

case class State(left: Jug, right: Jug)

implicit val stateOrder: Ordering[State] = Ordering.by(State.unapply)

case class Problem(initial: State, destination: State)

type StateMap = Map[State, Seq[State]]

def emptyJug(c: Int) = Jug(c, 0)

def newProblem(rc: Int, lc: Int, r: Int, l: Int) = Problem(State(emptyJug(rc), emptyJug(lc)), State(Jug(rc, r), Jug(lc, l)))

def forRightFull(st: State): Option[State] = {
    val State(Jug(rc, rh), lj) = st
    if (rc <= rh) None else Some(State(Jug(rc, rc), lj))
}

def forRightToLeft(st: State): Option[State] = {
    val State(Jug(rc, rh), Jug(lc, lh)) = st
    if (rh == 0 || lc < lh) None else {
        val maxCanPour = lc - lh
        val liquidToTransfer = if (maxCanPour >= rh) rh else maxCanPour
        Some(State(Jug(rc, rh - liquidToTransfer), Jug(lc, lh + liquidToTransfer)))
    }
}

def forRightToEmpty(st: State): Option[State] = {
    val State(Jug(rc, rh), lj) = st
    if (rh == 0) None else Some(State(emptyJug(rc), lj))
}

def interchange(fn: State => Option[State]): State => Option[State] = { st =>
    fn(st).map { case State(rj, lj) => State(lj, rj)}
}

val forLeftFull = interchange(forRightFull)
val forLeftToRight = interchange(forRightToLeft)
val forLeftToEmpty = interchange(forRightToEmpty)

def getNextState(st: State): Seq[State] = Seq(forRightToLeft _, forRightToEmpty _, forLeftFull,
                    forLeftToRight, forLeftToEmpty, forRightFull _).map(f => f(st)).flatten

def allStates(p: Problem): StateMap = {
    val Problem(i, f) = p
    def go(current: State, queue: Set[State], m: StateMap): StateMap = {
        if (queue.isEmpty) m else {
            val ns = getNextState(current)
            val mPrime = m + (current -> ns)
            val newQueue = ns.toSet &~ m.keySet
            val queuePrime = queue.union(newQueue)
            val next = queuePrime.head 
            go(next, queuePrime - next, mPrime)
        }
    }
    go(i, Set(i), Map.empty[State, List[State]])
}

def isPathPossible(p: Problem, stateMap: StateMap): Boolean = stateMap.values.flatten.iterator.contains(p.destination)

def findPaths(p: Problem, s: StateMap) : Seq[Seq[State]] = {
    def go(xs: Seq[State], ps: Seq[Seq[State]]): Seq[Seq[State]] = {
        if (xs.tail.contains(xs.head)) {
            ps
        } else if (xs.head == p.destination) { 
            xs +: ps
        } else if (!s.contains(xs.head)) {
            ps
        } else {
            s(xs.head).foldLeft(ps){(acc, l) => go(l +: xs, acc)}
        }
    }
    go(Seq(p.initial), Seq.empty[Seq[State]])
}

def shortestPath(st: Seq[Seq[State]]): Seq[State] = {
    st.tail.foldLeft(st.head) {(acc, xs) => if (xs.length < acc.length) xs else acc}
}

def solve(p: Problem): Option[Seq[State]] = {
    val ss = allStates(p)
    if (isPathPossible(p, ss)) {
        Some(shortestPath(findPaths(p, ss)))
    } else {
        None
    }
}

def solveAndShow(p: Problem) = {
    println(s"$p: ${solve(p)}")
}

solveAndShow(newProblem(4,3,2,2))

solveAndShow(newProblem(5,3,4,0))

solveAndShow(newProblem(4,3,2,0))