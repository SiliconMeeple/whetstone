from B import *

from unittest import TestCase


class BTests(TestCase):

    def go(self, problem):
        return solve(parse_problem(["42", problem]))

    def test_sample_one(self):
        self.assertEqual(self.go("3"), 3)

    def test_sample_two(self):
        self.assertEqual(self.go("1 2 1 2"), 2)

    def test_sample_three(self):
        self.assertEqual(self.go("4"), 3)

    def test_one_six(self):
        self.assertEqual(self.go("6"), 4)

    def test_one_one(self):
        self.assertEqual(self.go("1"), 1)

    def test_six_nine_sized_pancakes(self):
        self.assertEqual(self.go("9 9 9 9 9 9"), 9)

    def test_one_nine_sized_pancake(self):
        self.assertEqual(self.go("9"), 5)   # 9, 1 + 6 3, 2 + 3 3 3

    def test_two_nine_sized_pancake(self):
        self.assertEqual(self.go("9 9"), 7)

    def test_five_hundred_thousands(self):
        self.assertEqual(self.go("1000 " * 500), 1000)

    def test_nine_eight(self):
        self.assertEqual(self.go("9 8 "), 7)

    def test_four_six_1(self):
        self.assertEqual(self.go("4 6 1"), 5)

    def test_four_eights(self):
        self.assertEqual(self.go("8 8 8 8"), 8)

    def test_better_after_worse_eights(self):
        # Time taken goes: [8,7,8] = 8, [7,4,4,4,4] + 2= 9, [4,4,4,4,4,3] + 3 = 7
        self.assertEqual(self.go("8 7 8"), 7)