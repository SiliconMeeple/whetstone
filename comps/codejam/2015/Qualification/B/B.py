#!env python

import sys
from itertools import izip_longest
from math import ceil
from collections import defaultdict


def chunk(xs, n, fillvalue=None):
    args = [iter(xs)] * n
    return izip_longest(fillvalue=fillvalue, *args)


def parse_problem(lines):
    pancakes = map(int, lines[1].split())
    # Number of plates with N pancakes
    problem = defaultdict(int)
    for pancake in set(pancakes):
        problem[pancake] = pancakes.count(pancake)
    return problem


def solve(problem):
    special_minutes = 0
    lowest = sys.maxint
    while True:
        m = max(problem.keys())
        if m == 0:
            return special_minutes

        if special_minutes + m < lowest:
            lowest = m + special_minutes

        half_m = int(ceil(m / 2.0))
        count_of_m = problem[m]
        new_problem = defaultdict(int, problem)
        del new_problem[m]
        new_problem[half_m] += count_of_m
        new_problem[m-half_m] += count_of_m
        special_minutes += count_of_m
        problem = new_problem
        if m <= 3:
            return lowest

if __name__ == '__main__':
    sys.stdin.next()
    problems = chunk(sys.stdin, 2)
    for case, prob in enumerate(problems):
        print 'Case #{}: {}'.format(case + 1, solve(parse_problem(prob)))
