#!env python

import sys
from itertools import izip_longest


def chunk(xs, n, fillvalue=None):
    args = [iter(xs)] * n
    return izip_longest(fillvalue=fillvalue, *args)


def parse_problem(lines):
    l, x = lines[0].split()
    return lines[1][:-1] * int(x)


def split(line):
    for i_len in range(1, len(line) - 1):
        for j_len in range(1, len(line) - i_len):
            k_index = i_len + j_len
            yield (line[0:i_len], line[i_len:k_index], line[k_index:])

# m = -, p = -i, q=-j, o =-k
matrix = {('i', 'i'): 'm',
          ('p', 'p'): '',
          ('i', 'j'): 'k',
          ('p', 'q'): 'k',
          ('i', 'k'): 'q',
          ('p', 'o'): 'q',
          ('j', 'i'): 'o',
          ('q', 'p'): 'o',
          ('j', 'j'): 'm',
          ('q', 'q'): '',
          ('j', 'k'): 'i',
          ('q', 'o'): 'i',
          ('k', 'i'): 'j',
          ('o', 'p'): 'j',
          ('k', 'j'): 'p',
          ('o', 'q'): 'p',
          ('k', 'k'): 'm',
          ('o', 'o'): '',

          ('m', 'm'): '',

          ('m', 'p'): 'i',
          ('m', 'q'): 'j',
          ('m', 'o'): 'k',
          ('m', 'i'): 'p',
          ('m', 'j'): 'q',
          ('m', 'k'): 'o',

          ('p', 'm'): 'i',
          ('q', 'm'): 'j',
          ('o', 'm'): 'k',
          ('i', 'm'): 'p',
          ('j', 'm'): 'q',
          ('k', 'm'): 'o',
          }


def can_reduce_to(actual, target):
    while True:
        if len(actual) <= 1:
            return actual[0] == target
        fst, snd, rst = actual[0], actual[1], actual[2:]
        actual = matrix[(fst, snd)] + rst


def solve(problem):
    if len(problem) < 3:
        return "NO"
    if len(set(problem)) <= 1:
        return "NO"
    for i, j, k in split(problem):
        if can_reduce_to(i, "i") and can_reduce_to(j, "j") and can_reduce_to(k, "k"):
            return "YES"
    return "NO"


if __name__ == '__main__':
    sys.stdin.next()
    problems = chunk(sys.stdin, 2)
    for case, prob in enumerate(problems):
        print 'Case #{}: {}'.format(case + 1, solve(parse_problem(prob)))
