#!env pypy
# Runs instantly in pypy, blows the heap in python. Go figure!

import sys
from itertools import izip_longest, islice
from math import copysign

# i = 2, j = 3, k = 4
M = [[1, 2, 3, 4],
     [2, -1, 4, -3],
     [3, -4, -1, 2],
     [4, 3, -2, -1]]

def mul(o1, o2):
    sign = -1 if o1*o2 < 0 else 1
    return sign * M[abs(o1)-1][abs(o2)-1]

def chunk(xs, n, fillvalue=None):
    args = [iter(xs)] * n
    return izip_longest(fillvalue=fillvalue, *args)

def parse_problem(lines):
    l, x = lines[0].split()
    def generator():
        for i in range(int(x)):
            for c in lines[1][:-1]:
                yield ord(c) - ord('i') + 2
    return (int(x), lines[1][:-1], len(lines[1][:-1]), generator)

def can_find_ij_in(xs):
    i_accum = 1
    j_accum = 1
    for x in xs:
        if i_accum != 2:
            i_accum = mul(i_accum, x)
        elif j_accum != 3:
            j_accum = mul(j_accum, x)
        if i_accum == 2 and j_accum == 3:
            return True
    return False

def power(a, n):
    value = 1
    for i in range(n % 4):
        value = mul(value, a)
    return value

def solve(problem):
    l, s, len_s, generator = problem

    if power(reduce(mul, islice(generator(), len_s)), l) == -1 and can_find_ij_in(islice(generator(), len_s * min(l, 8))):
        return "YES"
    else:
        return "NO"


if __name__ == '__main__':
    sys.stdin.next()
    problems = chunk(sys.stdin, 2)
    for case, prob in enumerate(problems):
        print 'Case #{}: {}'.format(case + 1, solve(parse_problem(prob)))
