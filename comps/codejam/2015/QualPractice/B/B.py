#!env python

import sys
from itertools import izip_longest

def chunk(xs, n, fillvalue=None):
    args = [iter(xs)] * n
    return izip_longest(fillvalue=fillvalue, *args)

def parse_problem(lines):
    return map(int, lines[1].split())

def solve(problem):
    return min ([count_minutes(i, problem) for i in range(1, max(problem) + 1)])

def count_minutes(max_pancakes, problem):
    return sum([(P_i - 1) / max_pancakes for P_i in problem]) + max_pancakes

if __name__ == '__main__':
    sys.stdin.next()
    problems = chunk(sys.stdin, 2)
    for case, prob in enumerate(problems):
        print 'Case #{}: {}'.format(case + 1, solve(parse_problem(prob)))
