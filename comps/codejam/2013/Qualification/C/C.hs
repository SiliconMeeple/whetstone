module Main where

import Common
import Data.List (filter)

main = do getLine
          interact' go
          
go :: [Integer] -> Int
go [a , b] = length $ filter intPalindrome $ map (\x -> x*x) $ filter intPalindrome [(intSqrt ceiling a)..(intSqrt floor b)]
  
intPalindrome :: Integer -> Bool
intPalindrome = palindrome . show

intSqrt :: (Double -> Integer) -> Integer -> Integer
intSqrt f = f . sqrt . fromIntegral

palindrome :: (Eq a) => [a] -> Bool
palindrome xs = p [] xs xs
   where p rev (x:xs) (_:_:ys) = p (x:rev) xs ys
         p rev (x:xs) [_] = rev == xs
         p rev xs [] = rev == xs