module Main where

import Common (format)
import Debug.Trace
import Data.List (transpose, find)
import Data.Maybe (isJust, fromJust)

data Cell = X | O | T | E
                        deriving (Show, Eq)

toCell :: Char -> Cell
toCell 'X' = X
toCell 'O' = O
toCell '.' = E
toCell 'T' = T

main = do getLine
          interact (unlines . (zipWith format [1..]) . (map go . mangle . lines))
          
go :: [[Cell]] -> String          
go rows | isJust (winningPlayer lines) = (show (fromJust (winningPlayer lines))) ++ " won"
        | any (==E) (concat lines) = "Game has not completed"
        | otherwise = "Draw"
  where
    lines = rows ++ columns ++ diags rows
    columns = transpose rows
    winningPlayer = fmap winner . find (\ x -> winner x /= E)
      
winner :: [Cell] -> Cell      
winner xs | all (\x -> x == X || x == T) xs = X
          | all (\x -> x == O || x == T) xs = O
          | otherwise = E
    
diags :: [[a]] -> [[a]]
diags xs = [indexes xs [0..]] ++ [indexes xs [3,2..0]]
          where
            indexes xs ns = map (uncurry (!!)) $ zipWith ((,)) xs ns

mangle :: [String] -> [[[Cell]]]
mangle [] = []
mangle xs = map (map toCell) (take 4 xs) : mangle (drop 5 xs)