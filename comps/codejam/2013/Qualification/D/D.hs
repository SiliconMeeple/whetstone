module Main where

import Common
import Debug.Trace
import Data.List (intersperse)
import Data.Sequence (mapWithIndex)
import Data.Maybe
import Data.List

main = do getLine
          interact (unlines . (zipWith format [1..]) . (map go . mangle . map (map read) . map words . lines))
                    
                    
type Chest = (Int, Int, [Int])                    
                    
go :: ([Int], [Chest]) -> String
go (keys, chests) = concat $ find isJust $ (search keys chests []) ++ [Just "IMPOSSIBLE"]

search :: [Int] -> [Chest] -> [Int] -> [Maybe String]
search _ [] r = intersperse " " (map show r)
search [] _ _ = Nothing
search keys chests openChests = search (delete openedKey keys) (delete openedChest chests) (openChests ++ [chestId]) 
                                where openedKey = undefined
                                      openedChest = undefined
                                      chestId = undefined
                    
mangle :: [[Int]] -> [([Int], [(Int, [Int])])]                    
mangle [] = []
mangle ((_ : n : []) : initialKeys : rest ) = (initialKeys, mapWithIndex toChest (take n rest)) : mangle (drop n rest)
                                                       
toChest :: Int -> [Int] -> (Int, Int, [Int])
toChest i (k : [0]) = (i, k, [])
toChest i (k : contents) = (i, k, contents)