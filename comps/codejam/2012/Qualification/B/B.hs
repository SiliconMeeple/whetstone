import Common

main = do getLine
          interact' solve
          
solve (n : s : p : ts) = go p s 0 ts
go p s a (t:ts)  
  | score >= p = go p s (a+1) ts 
  | t == 0 = go p s a ts
  | rem /= 0 && score + 1 >= p = go p s (a+1) ts
  | s > 0 && rem == 0 && score + 1 >= p = go p (s-1) (a+1) ts
  | s > 0 && rem == 2 && score + 2 >= p = go p (s-1) (a+1) ts
  | otherwise = go p s a ts
  where score = t `div` 3  
        rem   = t `mod` 3
                      
go s p a [] = a