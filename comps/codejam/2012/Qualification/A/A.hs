import Common
import Data.Char (toLower)

main = do getLine 
          interact $ jam $ map (translateChar . toLower)

translateChar 'a' = 'y'
translateChar 'b' = 'h'
translateChar 'c' = 'e'
translateChar 'd' = 's'
translateChar 'e' = 'o'
translateChar 'f' = 'c'
translateChar 'g' = 'v'
translateChar 'h' = 'x'
translateChar 'i' = 'd'
translateChar 'j' = 'u'
translateChar 'k' = 'i'
translateChar 'l' = 'g'
translateChar 'm' = 'l'
translateChar 'n' = 'b'
translateChar 'o' = 'k'
translateChar 'p' = 'r'
translateChar 'q' = 'z'
translateChar 'r' = 't'
translateChar 's' = 'n'
translateChar 't' = 'w'
translateChar 'u' = 'j'
translateChar 'v' = 'p'
translateChar 'w' = 'f'
translateChar 'x' = 'm'
translateChar 'y' = 'a'
translateChar 'z' = 'q'
translateChar ' ' = ' '
translateChar _ = 'X'