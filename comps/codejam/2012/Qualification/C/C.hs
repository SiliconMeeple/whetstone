module Main where 

import Data.Char (digitToInt)
import Common
import qualified Data.Set as Set

main = do getLine
          interact' solveB
          
solveA :: [Integer] -> Int
solveA [lower , higher] = Set.size . Set.fromList $ [((min x n), (max x n)) | n <- [lower..higher], x <- permutations n, lower <= x, x <= higher, x /= n]
solveA _ = -1

permutations :: Integer -> [Integer]
permutations n = take (floor (logBase 10 (fromIntegral n))) (drop 1 (iterate permute n))

permute :: Integer -> Integer
permute n = (newB * m) + d
  where (d, m) = n `divMod` 10
        newB = floor (10 ** ((fromIntegral oldB)))
        oldB = floor (logBase 10 (fromIntegral n))
        
        
solveB :: [Integer] -> Int        
solveB [lower, higher] = length [1 | n <- [lower..higher], temp <- permutations n, temp > n, temp <= higher]