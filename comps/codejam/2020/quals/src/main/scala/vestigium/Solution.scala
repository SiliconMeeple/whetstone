package vestigium

import scala.io.Source

object Solution extends App {

  implicit class Chain[T](t: T) {
    def pipe[R](fn: T => R) =
      fn(t)
  }

  type Problem = Seq[Seq[Int]]

  def splitProblems(s: Iterator[String]): Seq[Problem] = {
    if (!s.hasNext) {
      Nil
    } else {
      val size = s.next.toInt

      s.take(size)
        .toList
        .map(_.split("\\s").toList.map(_.toInt)) +: splitProblems(s)
    }
  }

  def solve(problem: Problem): String = {
    val trace = sumDiagonal(problem)
    val rows = problem.count(hasRepeat)
    val columns = problem.transpose.count(hasRepeat)

    s"$trace $rows $columns"
  }

  def sumDiagonal(problem: Problem): Int = {
    problem.zipWithIndex.map { case (row, i) => row(i)}.sum
  }

  def hasRepeat(line: Seq[Int]): Boolean =
    line.size != line.distinct.size

  Source
    .stdin
    .getLines()
    .drop(1)
    .pipe(splitProblems)
    .map(solve)
    .zipWithIndex
    .foreach { case (s, i) =>
      println(s"Case #${i + 1}: $s")
    }

}
