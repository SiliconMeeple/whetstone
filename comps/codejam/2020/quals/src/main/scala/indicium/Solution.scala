package indicium

import scala.io.Source

object Solution extends App {

  type Problem = (Int, Int)

  type Row = Seq[Int]
  type Square = Seq[Row]

  def toProblem(s: String): Problem = {
    val Array(n, k) = s.split("\\s")
    n.toInt -> k.toInt
  }

  def solve(problem: Problem): String = {
    val (n, k) = problem

    val maybeAnswer = generateAllSquaresOfSize(n)
      .find(s => sumDiagonal(s) == k && !s.transpose.exists(r => hasRepeat(r)))
    maybeAnswer.fold("IMPOSSIBLE")(s => "POSSIBLE\n" + draw(s))
  }

  def draw(s: Square) = s.map(_.mkString(" ")).mkString("\n")

  def generateAllSquaresOfSize(i: Int): Stream[Square] = {
    def rowPermutation: Stream[Row] = (1 to i).permutations.toStream
    (2 to i).foldLeft(rowPermutation.map(x => Seq(x))){ (squareSoFar, _) =>
      rowPermutation.flatMap(row => squareSoFar.map { rest =>  row +: rest })
    }
  }

  def sumDiagonal(square: Square): Int = {
    square.zipWithIndex.map { case (row, i) => row(i)}.sum
  }


  def hasRepeat(line: Row): Boolean =
    line.size != line.distinct.size

  Source
    .stdin
    .getLines()
    .drop(1)
    .map(toProblem)
    .map(solve)
    .zipWithIndex
    .foreach { case (s, i) =>
      println(s"Case #${i + 1}: $s")
    }

}
