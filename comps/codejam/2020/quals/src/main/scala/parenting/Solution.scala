package parenting

import scala.io.Source

object Solution extends App {

  implicit class Chain[T](t: T) {
    def pipe[R](fn: T => R) =
      fn(t)
  }

  type Problem = Seq[(Int, Int, Int)]

  def splitProblems(s: Iterator[String]): Seq[Problem] = {
    if (!s.hasNext) {
      Nil
    } else {
      val size = s.next.toInt

      (s.take(size)
        .toList
        .map(_.split("\\s").toList.map(_.toInt))
        .zipWithIndex
        .map{case (s, i) => (s.head, s(1), i)}
        .sortBy(_._1) +: splitProblems(s))
    }
  }

  def solve(problem: Problem): String = {
    validSolution(problem).fold("IMPOSSIBLE"){s => s.sortBy(_._2).map(_._1).mkString}
  }

  def validSolution(problem: Problem, currentEnd1: Int = 0, currentEnd2: Int = 0): Option[Seq[(String, Int)]] = {
    problem match {
      case Nil => Some(Seq(("", 0)))
      case (start, end, i) +: rest =>
        if (start >= currentEnd1) {
          validSolution(rest, end, currentEnd2).map(("C", i) +: _)
        } else if (start >= currentEnd2) {
          validSolution(rest, currentEnd1, end).map(("J", i) +: _)
        } else None
    }
  }

  Source
    .stdin
    .getLines()
    .drop(1)
    .pipe(splitProblems)
    .map(solve)
    .zipWithIndex
    .foreach { case (s, i) =>
      println(s"Case #${i + 1}: $s")
    }

}
