package nesting


import scala.io.Source

object Solution extends App {

  implicit class Chain[T](t: T) {
    def pipe[R](fn: T => R) =
      fn(t)
  }

  type Problem = String

  def splitProblems(s: Iterator[String]): Seq[Problem] = s.toSeq

  def solve(problem: Problem): String = {
    go(problem, 0).mkString
  }

  def go(s: Seq[Char], currentNesting: Int): Seq[Char] = {
    s match {
      case Nil => List.fill(currentNesting)(')')
      case n +: rst =>
        val newNesting = n - '0'
        val diff = newNesting - currentNesting
        if (diff > 0)
          List.fill(diff)('(') ++ (n +: go(rst, newNesting))
        else {
          List.fill(-diff)(')') ++ (n +: go(rst, newNesting))
        }
    }
  }


  Source
    .stdin
    .getLines()
    .drop(1)
    .pipe(splitProblems)
    .map(solve)
    .zipWithIndex
    .foreach { case (s, i) =>
      println(s"Case #${i + 1}: $s")
    }

}
