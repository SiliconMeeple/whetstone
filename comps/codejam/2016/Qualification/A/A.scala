#!/bin/sh
exec scala -nc "$0" "$@"
!#

import scala.io.Source


object A {
    def main(args: Array[String]) =
        Source.stdin
                .getLines
                .drop(1)
                .map(line => problem(BigInt(line)))
                .zipWithIndex
                .foreach { case (s, i) =>
                    println(s"Case #${i+1}: $s")
                }


    def problem(n: BigInt) = {
        val digits = toDigits(n)
        solve(n, 1, n, digits.toSet)
    }

    def toDigits(n: BigInt): Vector[Int] = {
        n.toString().toVector.map(_ - '0')
    }

    val AllDigits = Set(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

    def solve(n: BigInt, i: Int, ni: BigInt, digits: Set[Int]): String = {
        if (digits == AllDigits) {
            ni.toString
        } else if (i >= 1000) {
            "INSOMNIA"
        } else {
            val newNi = n*(i+1)
            solve(n, i + 1, newNi, digits ++ toDigits(newNi))
        }

    }
}