name := "B"

scalaVersion := "2.11.8"

fork                  :=   true // Fork to separate process
connectInput in run   :=   true // Connects stdin to sbt during forked runs

outputStrategy        :=   Some(StdoutOutput) // Get rid of output prefix