import java.io.File

import scala.annotation.tailrec
import scala.io.Source

sealed trait Pancake

case object Happy extends Pancake

case object Blank extends Pancake

object B {
    def main(args: Array[String]) = {
        val pw = new java.io.PrintWriter(new File(args(1)))
        Source.fromFile(args(0))
                .getLines
                .drop(1)
                .map(line => solve(parse(line)))
                .zipWithIndex
                .foreach { case (s, i) =>
                    pw.println(s"Case #${i + 1}: $s")
                }
        pw.close
    }

    def parse(line: String): Vector[Pancake] = line.map {
        case '+' => Happy
        case '-' => Blank
    }.toVector


    def solve(pancakes: Vector[Pancake]) =
        go(pancakes, 0)

    def flipPancake(pancake: Pancake) = pancake match {
        case Happy => Blank
        case Blank => Happy
    }

    @tailrec def go(pancakes: Vector[Pancake], flips: Int): Int = {
        if (pancakes.forall(_ == Happy)) {
            flips
        } else {
            val lastBlankIndex = pancakes.indexOf(flipPancake(pancakes.head))
            if (lastBlankIndex == -1) {
                flips + 1
            } else {
                val newPrefix = pancakes.take(lastBlankIndex).map(flipPancake).reverse
                go(newPrefix ++ pancakes.drop(lastBlankIndex), flips + 1)

            }
        }
    }

}