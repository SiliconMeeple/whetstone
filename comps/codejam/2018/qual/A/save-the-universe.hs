module Main where

    import Debug.Trace

    countIf :: (a -> Bool) -> [a] -> Integer
    countIf cond = toInteger . length . filter cond

    displayResult :: Show a => (a, String) -> String
    displayResult (x,y) = "Case #" ++ (show x) ++ ": " ++ y

    displayResults :: [String] -> [String]
    displayResults xs = map displayResult $ zip [1..] xs
        
    showResult :: Maybe Int -> String
    showResult Nothing = "IMPOSSIBLE"
    showResult (Just n) = show n

    main :: IO ()
    main = do
        testCases <- getLine
        interact (unlines . displayResults . map showResult . map interpretAndSolve . lines)

    interpretAndSolve :: String -> Maybe Int
    interpretAndSolve s = let [ds, prog] = words s
                              d = (read ds) :: Integer
            in if countIf (== 'S') prog > d then Nothing else solve d prog 0

    solve :: Integer -> String -> Int -> Maybe Int
    solve d prog steps = case (interpret prog 1 0) of
                n | n <= d -> Just steps
                otherwise -> solve d (perturb prog) (steps + 1)

    interpret :: String -> Integer -> Integer -> Integer
    interpret [] s d = d
    interpret ('C':rst) s d = interpret rst (s*2) d
    interpret ('S':rst) s d = interpret rst s (d+s)

    perturb :: String -> String
    perturb = reverse . swapFirstSC . reverse
    
    swapFirstSC :: String -> String
    swapFirstSC ('S':'C':rst) = 'C':'S':rst
    swapFirstSC (x:xs) = x : (swapFirstSC xs)
    swapFirstSC [] = []