{-# LANGUAGE BangPatterns #-}

module Main where

    import Data.List
    import Debug.Trace
    import qualified Data.ByteString.Lazy.Char8 as B

    displayResult :: Show a => (a, B.ByteString) -> B.ByteString
    displayResult (x,y) = (B.pack "Case #") `B.append` (B.pack . show $ x) `B.append` (B.pack ": ") `B.append` y

    displayResults :: [B.ByteString] -> [B.ByteString]
    displayResults xs = map displayResult $ zip [1..] xs
        
    showResult :: Maybe Int -> B.ByteString
    showResult Nothing = B.pack "OK"
    showResult (Just n) = B.pack . show $ n

    every :: Int -> [a] -> [a]
    every n xs = case drop (n-1) xs of
        (y:ys) -> y : every n ys
        [] -> []

    main :: IO ()
    main = do
        testCases <- getLine
        B.interact $ B.unlines . displayResults . map showResult . map solve . map interpret . every 2 . B.lines

    interpret :: B.ByteString -> [Integer]
    interpret b = unfoldr step b
        where step !s = case B.readInteger s of 
                Nothing -> Nothing
                Just (!k, !t) -> Just (k, if B.null t then t else B.tail t)

    solve :: [Integer] -> Maybe Int
    solve vs = 
        if (sorted == troubled) then Nothing else Just (findFirstNonAscendingIndexIn troubled)
        where 
            sorted = sort vs
            troubled = troublesort vs

    troublesort :: [Integer] -> [Integer]
    troublesort xs =
        case (troublestep xs []) of 
            (False, xs')   -> troublesort (reverse xs')
            (True, result) -> reverse result

    troublestep :: [Integer] -> [Integer] -> (Bool, [Integer])
    troublestep (x:y:z:rst) acc | x > z  = (False, (reverse rst) ++ (x:y:z:acc))
    troublestep (x:xs) rst                      = troublestep xs (x:rst) 
    troublestep [] rst                          = (True, rst)

    findFirstNonAscendingIndexIn :: [Integer] -> Int
    findFirstNonAscendingIndexIn troubled = 
        length $ takeWhile ascending $ zip troubled (tail troubled)
                                    where ascending x = (fst x) <= (snd x)