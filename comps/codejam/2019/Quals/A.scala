import scala.io.Source

object Solution extends App {

    def parse(s: String) = s

    def solve(n: String) = {
        val a = n.replaceAll("4", "3")
        val b = (BigInt(n) - BigInt(a)).toString
        s"$a $b"
    }

     Source
         .stdin
         .getLines()
         .drop(1)
         .map(parse)
         .map(solve)
         .zipWithIndex
         .foreach { case (s, i) =>
             println(s"Case #${i + 1}: $s")
         }

}