import scala.io.Source

object Solution extends App {

    def parse(s: Seq[String]) = (s(0).toInt, s(1))

    def solve(problem: (Int, String)) = {
        val (size, path) = problem
        path.map {
            case 'E' => 'S'
            case 'S' => 'E'
        }
    }

     Source
         .stdin
         .getLines()
         .drop(1)
         .grouped(2)
         .map(parse)
         .map(solve)
         .zipWithIndex
         .foreach { case (s, i) =>
             println(s"Case #${i + 1}: $s")
         }

}