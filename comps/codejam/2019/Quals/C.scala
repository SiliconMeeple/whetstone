import scala.io.Source

object Solution extends App {

    val Zero = BigInt(0)

    def sieve(s: Stream[BigInt]): Stream[BigInt] =
        s.head #:: sieve(s.tail filter(_ % s.head != Zero))

    val allPrimes = sieve(Stream.iterate(BigInt(2)){_ + 1})
    
    def parse(s: Seq[String]) = (s(0).split(" ")(0), s(1).split(" ").toSeq.map(BigInt(_)))

    def findPrimeFactors(n: BitInt, primes: Seq[BigInt]) = {
        primes.find(n % _ == Zero).fold(throw new RuntimeException(s"No prime factors for $n")) { p => 
            Seq(n / p, p).sorted
        }
    }

    def solve(problem: (Int, Seq[BigInt])) = {
        val primes = allPrimes.takeWhile(_ < problem._1)
        problem.map(findPrimeFactors(_, primes))
        

    }

     Source
         .stdin
         .getLines()
         .drop(1)
         .grouped(2)
         .map(parse)
         .map(solve)
         .zipWithIndex
         .foreach { case (s, i) =>
             println(s"Case #${i + 1}: $s")
         }

}