#! env amm

import scala.io.Source

def solve(x: String) =
    go(x.toSeq.map(_.toInt - 48).reverse, Seq.empty[Int]).dropWhile(_ == 0).mkString

def go(s: Seq[Int], acc: Seq[Int]): Seq[Int] = {
    s match {
        case 0 +: 1 +: r => go(r, acc :+ 9)
        case 0 +: c +: r => go((c-1) +: r, acc :+ 9)
        case x +: y +: r if y > x => go((y-1) +: r, acc :+ 9)
        case x +: r => go(r, x +: acc)
        case _ => acc
    }
}

@main
def main(): Unit = {
    Source
        .stdin
        .getLines()
        .drop(1)
        .map(line => solve(line))
        .zipWithIndex
        .foreach { case (s, i) =>
            println(s"Case #${i + 1}: $s")
        }
}