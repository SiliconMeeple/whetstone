#! env amm

import $file.B

import $ivy.`org.scalatest::scalatest:3.0.1`

import org.scalatest.{time =>_, _}
import Matchers._

// Extras
B.solve("30") shouldBe "29"

// Samples:
B.solve("132") shouldBe "129"
B.solve("1000") shouldBe "999"
B.solve("7") shouldBe "7"
B.solve("111111111111111110") shouldBe ("99999999999999999")

// Extras

B.solve("1010") shouldBe "999"
B.solve("1111") shouldBe "1111"

// B-small-attempt0.in

B.solve("911") shouldBe "899"

import ammonite.ops._

for (line <- read.lines!(pwd/"B-small-attempt0.in")) {
    val result = B.solve(line)
    result.toSeq shouldBe result.toSeq.sorted
}

println("Ding!")