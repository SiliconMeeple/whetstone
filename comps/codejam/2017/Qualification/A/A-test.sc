#! env amm

import $file.A

import $ivy.`org.scalatest::scalatest:3.0.1`

import org.scalatest.{time =>_, _}
import Matchers._

def solve(s: String, i: Int) = {
    A.solve(s.toStream, i, 0)
}

solve("---+-++-", 3) shouldBe 3
solve("+++++", 4) shouldBe 0
solve("-----", 1) shouldBe 5
solve("-++-", 3) shouldBe 2
an [IllegalStateException] should be thrownBy solve("-+-+-", 4)
an [IllegalStateException] should be thrownBy solve("-+-+", 4)

println("Ding!")