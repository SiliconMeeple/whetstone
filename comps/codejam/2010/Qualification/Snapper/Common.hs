module Common where

displayResult :: Show a => (a, String) -> String
displayResult (x,y) = "Case #" ++ (show x) ++ ": " ++ y

displayResults :: [String] -> [String]
displayResults xs = map displayResult $ zip [1..] xs