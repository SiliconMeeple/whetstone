module Main where

displayResult :: Show a => (a, String) -> String
displayResult (x,y) = "Case #" ++ (show x) ++ ": " ++ y

displayResults :: [String] -> [String]
displayResults xs = map displayResult $ zip [1..] xs

interact' f = unlines . displayResults . map f . lines

main = do
         testCases <- getLine
         interact (unlines . displayResults . map solve . lines)


solve :: String -> String
solve s = let [n, k] = (map read $ words s)
              n2 = floor (2**(fromIntegral n))
          in if k /= 0 && k `mod` n2 == n2-1 then "ON" else "OFF"



test = unlines $ map solve ["1 0", "1 1", "4 0", "4 46", "4 47", "4 48"]