module Main where

displayResult (x,y) = "Case #" ++ (show x) ++ ": " ++ y

displayResults xs = map displayResult $ zip [1..] xs

interact' f = interact $ unlines . displayResults . map f . pair . lines

interpret = map read . words 

pair :: [a] -> [(a, a)]
pair [] = []
pair (a1 : a2 : as) = (a1, a2) : pair as
pair _ = error "pair can only be called on lists of length divisble by 2."

splitAtSum s xs = splitAt (findIndex s xs 0) xs
                  where findIndex s []       i = i
                        findIndex s (x : xs) i | s - x >= 0 = findIndex (s-x) xs i+1
                                               | otherwise = i

main = do
         testCases <- getLine
         interact' solve

solve :: (String, String) -> String
solve (l1, l2) = let [r, k, n] = interpret l1
                     groups = interpret l2
                 in show $ cashup r k groups 0

cashup 0 _ _ t = t
cashup r k g t = let (onRide, remainder) = splitAtSum k g
                 in cashup (r-1) k (remainder ++ onRide) (t + sum onRide) 


test = unlines $ map solve [("4 6 4","1 4 2 1"), ("100 10 1","1"), ("5 5 10","2 4 2 3 4 2 1 2 1 3")]