module Main where

displayResult (x,y) = "Case #" ++ (show x) ++ ": " ++ y

displayResults xs = map displayResult $ zip [1..] xs

interact' f = interact $ unlines . displayResults . map f . lines

main = do
         testCases <- getLine
         interact' solve

solve :: String -> String
solve s = let n : f : xs = (map read $ words s)
              ds = map (abs . (f-)) xs
              t  = foldl1 moddedGCD ds
              y  = f `mod` t
          in show $ if y == 0 then 0 else t - y


moddedGCD 0 0 = 0
moddedGCD a b = gcd a b

test = unlines $ map solve ["3 26000000 11000000 6000000", "3 1 10 11", "2 800000000000000000001 900000000000000000001", "5 7 9 11 13 15", "5 5 9 13 15 19", "3 74932155 74932155 83257950",  "2 11905610 68984277", "2 10 15", "3 10 15 20"]