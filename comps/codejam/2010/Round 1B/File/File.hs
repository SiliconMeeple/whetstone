module Main where

import Common
import qualified Data.ByteString as BS
import qualified Data.Set as Set
import Data.List

type Path = [String]

main = do
         testCases <- getLine
         interact (displayResults . breakup . lines)

breakup :: [String] -> [Int]
breakup [] = []
breakup (cases : rest) = let [n, m] = interpret cases
                             existingPaths = take n rest
                             requiredPaths = take m (drop n rest)
                             solvedRest = breakup $ drop (n+m) rest
                         in
                             solve (map turnIntoPath existingPaths) (map turnIntoPath requiredPaths) : solvedRest


turnIntoPath :: String -> Path
turnIntoPath [] = []
turnIntoPath xs = tail (turnIntoPath' xs)

turnIntoPath' :: String -> Path
turnIntoPath' [] = []
turnIntoPath' xs = (if (last dir) == '/' then (init dir) else dir) : (turnIntoPath' rest)
                   where (dir, rest) = split (findIndex (=='/') xs) xs
                         split Nothing xs = (xs, [])
                         split (Just i) xs = splitAt (i+1) xs

solve :: [Path] -> [Path] -> Int
solve existing required = Set.size $ (buildSet required) `Set.difference` (buildSet existing)

buildSet :: [Path] -> Set.Set Path
buildSet = foldl' addPaths Set.empty
           where addPaths a p = foldl' (flip Set.insert) a (tail (inits p))