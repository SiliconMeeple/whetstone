module Main where

import Common
import Data.List
import Data.Maybe
import Debug.Trace

main = do
         testCases <- getLine
         interact' solve

solve :: (Integral a) => [a] -> a
solve xs = let [n] = xs
               validSets = filter (pure n) (subsequences [2..n])
           in (genericLength $ trace (show validSets) validSets) `mod` 100003


pure :: Integral a => a -> [a] -> Bool
pure 1 _  = True
pure n xs = if isNothing e then False else pure (((fromIntegral . fromJust) e) + 1) xs
             where e = elemIndex n xs