module Main where

import Common
import Debug.Trace

main = do
         testCases <- getLine
         interact' tries

tries :: Integral a => [a] -> a
tries x@[l, p, c] | l * c >= p = 0
                  | otherwise  = 1 + maximum (map tries (possibilities x))

phi = (1 + sqrt 5) / 2

possibilities :: Integral a => [a] -> [[a]]
possibilities [l, p, c] = let mp = midpoint (l * c) (p `div` c) c
                              fl = floor mp
                              ce = ceiling mp
                          in
                             if ce == fl then
                                             [[fl, p, c], [l, fl ,c]]
                                         else
                                             [[fl, p, c], [ce, p, c], [l, fl, c] , [l, ce, c]]



midpoint a b c= fromIntegral a + (fromIntegral (b - a) / phi)