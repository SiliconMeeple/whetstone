module Main where

import Common

main = do
          testCases <- getLine
          interactWith findProblem solve

type Point = [Double]

findProblem :: [String] -> [[Point]]
findProblem [] = []
findProblem (cases : rest) = let [n] = interpret cases
                             in map interpret (take n rest) : findProblem (drop n rest)


solve :: [Point] -> Int
solve [] = 0
solve (first : rest) = sum (map (clamp . findIntersection first) rest) + (solve rest)

clamp :: Double -> Int
clamp t = if (t < 0) || (t > 1) then 0 else 1


-- Solving the two parametric lines from l1 = P1 + t1(P2-P1), l2 = P3 + t2(P3-P4)
-- (t1 and t2 work out the same)
-- http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/
findIntersection [a1, b1] [a2, b2] = (a1-a2) / (b2 + a1 - a2 - b1)

