module Common where

displayResult :: (Show a, Show b) => (a, b) -> String -> String
displayResult (x,y) = ("Case #" ++) . (shows x) . (": " ++) . (shows y) . ("\n"++)

displayResults :: Show a => [a] -> String
displayResults xs = foldr displayResult "" $ zip [1..] xs

interactWith :: Show r => ([String] -> [a]) -> (a -> r) -> IO ()
interactWith m f = interact $ displayResults . map f . m . lines

interact' :: (Read a, Show r) => ([a] -> r) -> IO ()
interact' = interactWith $ map interpret

interpret :: Read a => String -> [a]
interpret = map read . words 

splitEvery :: Int -> [a] -> [[a]]
splitEvery _ [] = []
splitEvery i xs = [take i xs] ++ splitEvery i (drop i xs)