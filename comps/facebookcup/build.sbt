name := "facebook hackers cup"
libraryDependencies ++=
  Seq(
    "com.beachape" %% "enumeratum" % "1.6.1",
    "org.scalatest" %% "scalatest" % "3.1.0" % "test"
  )

scalaVersion := "2.13.6"