package fhc2021

import java.nio.charset.StandardCharsets.UTF_8
import java.time.{Duration, Instant}
import scala.util.Using

object HackerCupUtils {

  var debug = false

  def log(message: => String): Unit = {
    if (debug) {
      println(message)
    }
  }

  def problemSet(problemSet: String, problem: String)(solver: Seq[String] => String): Unit = {
    import java.nio.file.{Files, Paths}

    Using.resource(scala.io.Source.fromFile(s"src/main/resources/2021/$problemSet/$problem.input")) { contents =>
      val lines = contents.getLines.toArray
      val started = Instant.now()
      val result = solver(lines)

      log(
        s"""
          |$problemSet, $problem:
          |$result#
          |Took ${Duration.between(started, Instant.now())}
          |""".stripMargin
      )

      Files.write(Paths.get(s"src/main/resources/2021/$problemSet/$problem.output"), result.getBytes(UTF_8))
    }
  }


}
