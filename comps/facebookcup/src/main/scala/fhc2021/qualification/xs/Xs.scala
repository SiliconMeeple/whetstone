package fhc2021.qualification.xs


import fhc2021.{Grid, Vec}
import fhc2021.HackerCupUtils._

object Xs {

  case class Problem(grid: Grid[Char])

  def parse(lines: Seq[String]): Seq[Problem] = {
    parseProblems(lines.tail)
  }

  private def parseProblems(lines: Seq[String]): Seq[Problem] = {
    lines match {
      case Nil => Nil
      case length +: rest =>
        val l = length.toInt
        Problem(makeGrid(rest.take(l))) +: parseProblems(rest.drop(l))
    }
  }

  private def makeGrid(value: Seq[String]): Grid[Char] =
    Grid(value.map(_.toCharArray).toArray)

  def solveAll(problems: Seq[Problem]): String = {
    problems.zipWithIndex.map { case (p, i) =>
      val r = s"Case #${i + 1}: ${solve(p)}"
      log(r)
      r
    }.mkString("\n")
  }

  private def xsNeededForLine(line: Seq[(Char, Vec)]): Seq[Vec] =
    if (line.exists(_._1 == 'O')) Seq.empty
    else line.collect { case ('.', v) => v }

  private def solve(p: Problem): String = {

    log(p.grid.render())

    val r = (p.grid.rowsWithIndices ++ p.grid.columnsWithIndices).map(xsNeededForLine).groupBy(_.size)

    log(r.toString)

    if (r.size == 1 && r.contains(0)) {
      "Impossible"
    } else {
      val (needed, linesOfLocs) = r.removed(0).minBy(_._1)
      s"$needed ${linesOfLocs.toSeq.distinct.size}"
    }
  }

  def main(args: Array[String]): Unit = {
    problemSet("xs", "validation") { lines => solveAll(parse(lines))}
    problemSet("xs", "full") { lines => solveAll(parse(lines))}
  }
}
