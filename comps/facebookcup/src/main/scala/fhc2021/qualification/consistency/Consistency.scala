package fhc2021.qualification.consistency

import fhc2021.HackerCupUtils._

import scala.collection.mutable
import scala.util.control.Exception

object Consistency {

  type Replacements = Map[Char, Seq[Char]]

  case class Problem(s: String, replacements: Replacements)

  private val chapter1Replacements = {
    val vowels = Seq('A', 'E', 'I', 'O', 'U')
    val consonants = ('A' to 'Z').filterNot(vowels.contains)

    (vowels.map(_ -> consonants) ++ consonants.map(_ -> vowels)).toMap
  }

  def parseChapter1(strings: Seq[String]): Seq[Problem] = {
    strings.tail.map(Problem(_, chapter1Replacements))
  }

  def parseChapter2(strings: Seq[String]): Seq[Problem] = {
    parseChapter2Problems(strings.tail)
  }

  private def parseChapter2Problems(value: Seq[String]): Seq[Problem] = {
    value match {
      case Nil => Seq.empty
      case s +: count +: rest =>
        Problem(s, parseReplacements(rest.take(count.toInt))) +: parseChapter2Problems(rest.drop(count.toInt))
    }
  }

  def parseReplacements(values: Seq[String]): Replacements =
    values.groupBy(_ (0))
      .view
      .mapValues(_.map(_ (1)))
      .toMap

  def depthOfFirstOccurrance(first: Char, target: Char, replacements: Replacements, maxDepth: Int): Option[Int] = {
    val queue = mutable.Queue[(Char, Int)]()
    val visited = mutable.Set[Char]()
    queue.enqueue(first -> 0)

    while (queue.nonEmpty) {
      val (char, depth) = queue.dequeue()
      if (char == target) {
        return Some(depth)
      } else if (depth + 1 >= maxDepth) {
        return None
      } else if (replacements.contains(char)) {
        val newLetters = replacements(char).filterNot(visited.contains)
        queue.enqueueAll(newLetters.map(_ -> (depth + 1)))
        visited.addAll(newLetters)
      }
    }
    None
  }

  def lengthOfSmallestChain(from: Char, to: Char, replacements: Replacements): Int = {
    val maxLengthOfPath = replacements.keys.size + 1
    if (from == to) {
      0
    } else if (replacements(from).contains(to)) {
      1
    } else {
      depthOfFirstOccurrance(from, to, replacements, maxLengthOfPath).get
    }
  }


  def solve(p: Problem): Int = {
    val alphabet = (p.s.toSeq ++ p.replacements.keys ++ p.replacements.values.flatten).distinct

    alphabet
      .map { aimFor =>
        Exception.catching(classOf[NoSuchElementException]).opt {
          p.s.map(letter => lengthOfSmallestChain(letter, aimFor, p.replacements)).sum
        }
      }.collect { case Some(i) => i }
      .minOption
      .getOrElse(-1)
  }

  def solveAll(problems: Seq[Problem]): String = {
    problems.zipWithIndex.map { case (p, i) =>
      val r = s"Case #${i + 1}: ${solve(p)}"
      log(r)
      r
    }.mkString("\n")
  }

  def main(args: Array[String]): Unit = {
    problemSet("consistency", "chapter-1-validation") { lines => solveAll(parseChapter1(lines)) }
    problemSet("consistency", "chapter-1") { lines => solveAll(parseChapter1(lines)) }
    problemSet("consistency", "chapter-2-validation") { lines => solveAll(parseChapter2(lines)) }
    problemSet("consistency", "chapter-2") { lines => solveAll(parseChapter2(lines)) }
  }
}
