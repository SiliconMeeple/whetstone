package fhc2021


/** Stolen from AOC code */

import Vec.Directions

import scala.language.implicitConversions
import scala.reflect.ClassTag
import scala.util.hashing.MurmurHash3

case class Vec(var x: Int, var y: Int) {
  def clampX(width: Int): Vec =
    if (x < width) this else Vec(x - width, y)

  def +(o: Vec): Vec = Vec(x+o.x, y+o.y)
  def +=(o: Vec): Vec = {
    x += o.x
    y += o.y
    this
  }
  def unary_- : Vec = Vec(-x, -y)
  def -(o: Vec): Vec = this +(-o)
  def -=(o: Vec): Vec = {
    x -= o.x
    y -= o.y
    this
  }

  def *(i: Int): Vec =
    Vec(x * i, y * i)

  def adjacentVecs(implicit adjacency: Vec.Adjacency): Seq[Vec] =
    adjacency.adjacentDirections.map(this + _)

  def projectInAllDirections()(implicit adjacency: Vec.Adjacency):Seq[(Vec, LazyList[Vec])] =
    adjacency.adjacentDirections.map(dir =>
      dir -> LazyList.iterate(this + dir)(_ + dir)
    )

  def distanceFrom(o: Vec): Int = {
    val t = (this - o).abs
    t.x + t.y
  }

  def decimalDistanceFrom(o: Vec): Double = {
    (this - o).magnitude
  }

  def abs: Vec = Vec(math.abs(x), math.abs(y))

  def directionTo(o: Vec): (Double, Double) =
    (o - this).norm

  def norm: (Double, Double) =
    (toSensibleDp(x / magnitude), toSensibleDp(y / magnitude))

  def magnitude: Double =
    Math.sqrt(x * x + y * y)

  def manhattanMagnitude: Int = {
    val a = abs
    a.x + a.y
  }

  private def toSensibleDp(d: Double): Double = {
    BigDecimal(d).setScale(10, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  import Directions._

  def left: Vec = this match {
    case Up => Left
    case Left => Down
    case Down => Right
    case Right => Up
  }

  def right: Vec = this match {
    case Up => Right
    case Right => Down
    case Down => Left
    case Left => Up
  }
}

case class Grid[T](arr: Array[T], width: Int) {

  def this(a: Array[Array[T]])(implicit ct: ClassTag[T]) =
    this(a.flatten, a.head.length)

  private def vecToIndex(v: Vec): Int =
    (v.y * width) + v.x

  private def indexToVec(i: Int): Vec =
    (i % width, i / width)

  def apply(v: Vec): T =
    apply(v.x, v.y)

  def apply(x: Int, y: Int): T =
    arr((y * width) + x)

  def clampedGet(v: Vec): Option[T] =
    Option.when(inBounds(v))(apply(v))

  def update(v: Vec)(updateFn: T => T): Unit = {
    val i = vecToIndex(v)
    arr(i) = updateFn(arr(i))
  }

  def height: Int =
    arr.length / width

  def dimensions: (Int, Int) =
    (width, height)

  def inBounds(v: Vec): Boolean =
    v.x >= 0 && v.x < width && v.y >= 0 && v.y < height

  def render(renderElement: T => Char = _.toString.charAt(0)): String =
    arr.map(renderElement).grouped(width).map(_.mkString).mkString("\n")

  override def clone(): Grid[T] = Grid(arr.clone(), width)

  def set(v: Vec, t: T): Grid[T] = {
    arr(vecToIndex(v)) = t
    this
  }

  def dropRows(rows: Int) =
    new Grid(arr.drop(rows * width), width)

  def indices: Iterable[Vec] =
    for {
      x <- 0 until width
      y <- 0 until height
    } yield Vec(x, y)

  override def toString = s"Grid(Array(${arr.toSeq.mkString(",")}), ${width})"

  def map[B: ClassTag](f: T => B): Grid[B] =
    copy(arr = arr.map(f))

  def zipWithIndicies: Grid[(T, Vec)] =
    copy(arr = arr.zipWithIndex.map { case (t, i) => (t, indexToVec(i)) })

  def mapWithIndicies[B: ClassTag](f: (T, Vec) => B): Grid[B] =
    copy(arr = arr.zipWithIndex.map { case (t, i) => f(t, indexToVec(i)) })

  def foreachWithIndicies[B: ClassTag](f: (T, Vec) => B): Unit =
    arr.zipWithIndex.map { case (t, i) => f(t, indexToVec(i)) }

  override def equals(obj: Any): Boolean = obj match {
    case obj: Grid[T] =>
      width == obj.width &&  arr.sameElements(obj.arr)
    case _ => super.equals(obj)
  }

  override def hashCode(): Int =
    (31 * width) + (31 * MurmurHash3.orderedHash(arr))

  def topEdge: Seq[T] = arr.slice(0, width)
  def rightEdge: Seq[T] = (width - 1 until arr.length by width).map(arr)
  def bottomEdge: Seq[T] = arr.slice(arr.length - width, arr.length)
  def leftEdge: Seq[T] = (0 until arr.length - width + 1 by width).map(arr)

  // Top, Right, Bottom, Left
  def edges: Seq[Seq[T]] = {
    Seq(
      topEdge,
      rightEdge,
      bottomEdge,
      leftEdge
    )
  }

  def flipHorizontally: Grid[T] =
    copyAndRearrange{case Vec(x, y) => Vec(width - 1 - x, y)}

  def flipVertically: Grid[T] =
    copyAndRearrange{case Vec(x, y) => Vec(x, height - 1 - y)}

  def rotate90: Grid[T] =
    copyAndRearrange{case Vec(x, y) => Vec(height - 1 - y, x)}

  private def copyAndRearrange(fn: Vec => Vec): Grid[T] = {
    val newArr = arr.clone()
    arr.indices.foreach { i =>
      val v = indexToVec(i)
      newArr(vecToIndex(fn(v))) = arr(i)
    }
    new Grid(newArr, width)
  }

  def existsRow(p: T => Boolean): Boolean =
    (0 until height).exists(i => (0 until width).forall(j => p(apply(i, j))))

  def existsColumn(p: T => Boolean): Boolean =
    (0 until width).exists(j => (0 until height).forall(i => p(apply(i, j))))

  def rows: Iterable[Seq[T]] =
    arr.grouped(width)
      .map(_.toSeq)
      .to(Iterable)

  def columns: Iterable[Seq[T]] = {
    (0 until width by 1).map(i =>
      (i until arr.length by width).map(arr)
    )
  }

  def rowsWithIndices: Iterable[Seq[(T, Vec)]] =
    (0 until height).map(i =>
      (0 until width).map(j => apply(i, j) -> (i, j))
    )


  def columnsWithIndices: Iterable[Seq[(T, Vec)]] =
    (0 until width).map(j =>
      (0 until height).map(i => apply(i, j) -> (i, j))
    )

}

object Grid {
  def apply[T](a: Array[Array[T]])(implicit ct: ClassTag[T]) =
    new Grid(a)

  def apply[T](s: Seq[Seq[T]])(implicit ct: ClassTag[T]) =
    new Grid(s.map(_.toArray).toArray)

  def fill[T](width: Int, height: Int, t: T)(implicit ct: ClassTag[T]): Grid[T] =
    Grid(Array.fill(width * height)(t), width)

  def sequence[T](grids: Seq[Grid[T]]): Grid[Seq[T]] = {
    require(grids.map(_.dimensions).distinct.size == 1, s"Grids have different sizes: ${grids.map(_.dimensions).distinct}")

    val head = grids
      .head

    val newArr = head
      .arr
      .indices
      .map(i => grids.map(_.arr(i)))
      .toArray

    new Grid(newArr, head.width)
  }

  implicit class RichGridOfGrids[T](grid: Grid[Grid[T]])(implicit ct: ClassTag[T]) {
    def flatten: Grid[T] = {
      require(grid.rows.forall(_.map(_.width).sum == grid.rows.head.map(_.width).sum), s"Unequal rowcount: ${grid.rows.map(_.map(_.width).sum)}")
      require(grid.columns.forall(_.map(_.height).sum == grid.columns.head.map(_.height).sum), s"Unequal columncount: ${grid.columns.map(_.map(_.height).sum)}")

      val newWidth = grid.rows.head.map(_.width).sum
      val newHeight = grid.columns.head.map(_.height).sum
      val newArr = new Array[T](newWidth * newHeight)
      val newGrid = new Grid(newArr, newWidth)

      var top = 0
      grid.rows.foreach { row =>
        var left = 0
        row.foreach { grid =>
          for (i <- 0.until(grid.height)) {
            Array.copy(grid.arr, i * grid.width, newArr, newGrid.vecToIndex((left, top + i)), grid.width)
          }
          left += grid.width
        }
        top += row.head.height
      }
      newGrid
    }
  }
}

object Vec {
  val origin: Vec = Vec(0,0)

  implicit def tupleToVec(x: (Int, Int)): Vec = Vec(x._1, x._2)
  implicit def vecToTuple(v: Vec): (Int, Int) = (v.x, v.y)

  type Direction = (Int, Int)

  object Direction {
    def fromString(s: String): Vec = s match {
      case "R" => Directions.Right
      case "L" => Directions.Left
      case "U" => Directions.Up
      case "D" => Directions.Down
    }
  }

  implicit class DoubleVecWithDecimalDirectionFromPosYAxis(v: (Double, Double)) {

    def decimalDirectionFromPosYaxis: Double = {
      val ans = -Math.atan2(-v._1, -v._2)
      if (ans < 0) (2 * Math.PI) + ans else ans
    }

  }

  object Directions {
    val Up: Vec = Vec(0, 1)
    val Down: Vec = Vec(0, -1)
    val Left: Vec = Vec(-1, 0)
    val Right: Vec = Vec(1, 0)
  }

  sealed trait Adjacency {
    val adjacentDirections: Seq[Vec]
  }

  object OrthogonalAdjacency {
    implicit val orthogonalAdjacency: Adjacency = new Adjacency {
      override val adjacentDirections: Seq[Vec] = Seq((-1,0), (0,-1), (0, 1), (1, 0))
    }
  }

  object EightDirectionAdjacency {
    implicit val eightDirectionAdjacency: Adjacency = new Adjacency {
      override val adjacentDirections: Seq[Vec] = for {x <- Seq(-1, 0, 1); y <- Seq(-1, 0, 1) if x != 0 || y != 0} yield Vec(x,y)
    }
  }

  val orderingVec: Ordering[Vec] = Ordering.by((vecToTuple _).andThen(_.swap))

  implicit val numericVec: Numeric[Vec] = new Numeric[Vec] {
    override def plus(x: Vec, y: Vec): Vec = x + y

    override def minus(x: Vec, y: Vec): Vec = x - y

    override def negate(x: Vec): Vec = -x

    override def fromInt(x: Int): Vec = Vec(x,x)

    override def toInt(x: Vec): Int = ???

    override def toLong(x: Vec): Long = ???

    override def toFloat(x: Vec): Float = ???

    override def toDouble(x: Vec): Double = ???

    override def times(x: Vec, y: Vec): Vec = ???

    override def parseString(str: String): Option[Vec] = ???

    override def compare(x: Vec, y: Vec): Int = orderingVec.compare(x, y)
  }
}