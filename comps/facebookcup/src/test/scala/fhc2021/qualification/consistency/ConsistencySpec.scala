package fhc2021.qualification.consistency

import fhc2021.qualification.consistency.Consistency._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.util.Using

class ConsistencySpec extends AnyFunSuite with Matchers {

  test("It should work on the chapter 1 examples") {
    val sample =
      """6
        |ABC
        |F
        |BANANA
        |FBHC
        |FOXEN
        |CONSISTENCY""".stripMargin.split("\n")

    solveAll(parseChapter1(sample)) shouldBe
      """Case #1: 2
        |Case #2: 0
        |Case #3: 3
        |Case #4: 4
        |Case #5: 5
        |Case #6: 12""".stripMargin
  }

  test("It should work on the chapter 2 examples") {
    Using.resources(
      scala.io.Source.fromFile(s"src/main/resources/2021/consistency/chapter-2-sample.input"),
      scala.io.Source.fromFile(s"src/main/resources/2021/consistency/chapter-2-sample.output")) { (input, expected) =>
      val lines = input.getLines.toArray

      solveAll(parseChapter2(lines)) shouldBe expected.getLines.mkString("\n")
    }
  }
}
