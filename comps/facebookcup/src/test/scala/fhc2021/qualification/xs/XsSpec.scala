package fhc2021.qualification.xs


import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.util.Using

import Xs._

class XsSpec extends AnyFunSuite with Matchers {

  test("It should work on the chapter 2 examples") {
    Using.resources(
      scala.io.Source.fromFile(s"src/main/resources/2021/xs/sample.input"),
      scala.io.Source.fromFile(s"src/main/resources/2021/xs/sample.output")) { (input, expected) =>
      val lines = input.getLines.toArray

      solveAll(parse(lines)) shouldBe expected.getLines.mkString("\n")
    }
  }

}
