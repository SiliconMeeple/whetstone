package example

// http://programmingpraxis.com/2015/06/19/nines-and-zeros/
// Mostly inspired by the haskell solutions.

object App {
  def main(args: Array[String]) {
  	println(numbersMadeOfNineAndZero.take(10).toList);
  	println("Smallest number made of 9s and 0s that is divisible by 23:" + 	smallestDivisible(23))
  	println("Smallest number made of 9s and 0s that is divisible by 23:" + 	smallestDivisible2	(23))
  }

  def smallestDivisible(target: Int): Int =
  	numbersMadeOfNineAndZero.find(_ % target == 0).head

  def smallestDivisible2(target: Int): Int =
  	numbersMadeOfNineAndZero2.find(_ % target == 0).head

  lazy val numbersMadeOfNineAndZero: Stream[Int] = {
  	lazy val tens = numbersMadeOfNineAndZero.map(_ * 10)
  	9 #:: interleave(tens, tens.map(_ + 9))
  }

  def interleave[A](s: Stream[A], s2: Stream[A]): Stream[A] = 
  	s.head #:: interleave(s2, s.tail)

  lazy val numbersMadeOfNineAndZero2: Stream[Int] = {
  	lazy val f: (Stream[Int], Stream[Int]) => Stream[Int] = {
  		case (Stream.Empty, t) => f (t.reverse, Stream.empty)
  		case (n #:: s, t) => {
  			val m = n*10
  			n #:: f(s, (m+9) #:: m #:: t)
  		}
  	}
  	f(Stream(9), Stream.empty)
  }

}