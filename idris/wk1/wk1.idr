palindrome: String -> Bool
palindrome x = x == (reverse x)


palindromei: String -> Bool
palindromei x = let l = (toLower x) in
                    l == (reverse l)


longPalindrome: String -> Bool
longPalindrome s = (length s) > 10 && s == (reverse s)


palindromeN: Nat -> String -> Bool
palindromeN n s = (length s) > n && s == (reverse s)

counts: String -> (Nat, Nat)
counts s = (ws, cs) where
              ws = length (words s)
              cs = length s

backwards: Ordering -> Ordering
backwards o = o

top_ten: Ord a => List a -> List a
top_ten ls = take 10 (ss) where
              ss: List a
              ss = reverse $ sort ls

over_length: Nat -> List String -> Nat
over_length n ss = length $ filter pred ss
                     where pred : String -> Bool
                           pred = ((> n) . length)
