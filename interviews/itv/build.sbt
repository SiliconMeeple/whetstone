name := """minimal-scala"""

version := "1.0"

scalaVersion := "2.11.7"

// Change this to another test framework if you prefer
libraryDependencies ++= Seq("org.scalaz" %% "scalaz-core" % "7.1.3",
                            "org.scalatest" %% "scalatest" % "2.2.4" % "test",
                            "org.scalacheck" %% "scalacheck" % "1.12.2" % "test")

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

