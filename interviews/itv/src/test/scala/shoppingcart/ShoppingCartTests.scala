package shoppingcart

import org.scalatest.{Matchers, FlatSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import scala.util.Random
import scalaz.Scalaz.unfold

import ShoppingCart._

import scalaz.{NonEmptyList, Success, Failure}

class ShoppingCartTests extends FlatSpec with GeneratorDrivenPropertyChecks with Matchers {
  import shoppingcart.ShoppingCartTests._
  behavior of "A shopping cart"

  it should "be free when empty" in {
    priceWithTestSkus(List.empty[Sku]) should be (Success(0))
  }

  it should "correctly price a shopping cart" in {
    forAll { (totalPrice : Int) =>
      whenever(totalPrice > 0 && totalPrice < Integer.MAX_VALUE ) {
        val shoppingList = shoppingListFrom(totalPrice, testItems)
        priceWithTestSkus(shoppingList) should be (Success(totalPrice))
      }
    }
  }

  it should "fail to validate if an unrecognised SKU is passed in" in {
    forAll { (unknownSku : Sku) =>
      whenever(unknownSku != "") {
        price(Map.empty)(List(unknownSku)) should be (Failure(NonEmptyList(s"No price for SKU $unknownSku")))
      }
    }
  }

  it should "apply a bogof deal" in {
    def bogof(quantity: Int) = (quantity + 1)/ 2
    price(Map("A" -> bogof, "B" -> simplePrice(10)))(List("A", "B", "A", "B", "A")) should be (Success(22))
  }

  it should "apply a flat price" in {
    def flatPrice(quantity : Int) = 10
    price(Map("A" -> flatPrice))(List.fill(100)("A")) should be (Success(10))
  }

}

object ShoppingCartTests {
  def simplePrice(price: Int)(amount: Int) = price * amount

  val testItems: Map[Sku, PricingRule] =
    Map("A" -> 1, "B" -> 10, "C" -> 100,
      "D" -> 1000, "E" -> 10000, "F" -> 100000,
      "G" -> 1000000, "H" -> 10000000, "I" -> 100000000).mapValues(price => simplePrice(price))

  def priceWithTestSkus = price(testItems) _

  def shoppingListFrom(totalPrice: Int, items: Map[Sku, PricingRule]) = {
    def step(price: Int): Option[(String, Int)] = {
      if (price == 0)
        None
      else {
        val item = pickRandomElement(items.filter{ case (sku, rule) => rule(1) <= price })
        Some((item._1, price - item._2(1)))
      }
    }
    unfold(totalPrice)(step)
  }

  def pickRandomElement[A,B](coll: Map[A,B]): (A,B) = coll.toList(Random.nextInt(coll.size))

}
