package shoppingcart

import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}
import shoppingcart.PricingRules._

class PricingRuleTests extends FlatSpec with GeneratorDrivenPropertyChecks with Matchers {
  behavior of "pricePerUnit"

  it should "correctly price all units" in {
    pricePerUnit(13)(23) should be(299)
  }

  behavior of "specialPriceForM"

  it should "correctly price individual units" in {
    specialPriceForM(Integer.MAX_VALUE, 1, 10)(30) should be(300)
  }

  it should "correctly apply the deal when m = 1" in {
    specialPriceForM(1, 10, 1)(30) should be(300)
  }

  it should "correctly price 3 for 100, remainder at 50 each" in {
    specialPriceForM(3, 100, 50)(8) should be(300)
  }
}
