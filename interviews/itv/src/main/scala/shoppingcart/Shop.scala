package shoppingcart

import shoppingcart.ShoppingCart.{Sku, PricingRule, price}
import PricingRules._
import scala.util.Random

object Shop extends App {
  val shopItems: Map[Sku, PricingRule] = Map(
    "A" -> specialPriceForM(m = 3, dealPrice = 130, remainderPrice = 50),
    "B" -> specialPriceForM(m = 2, dealPrice = 45, remainderPrice = 30),
    "C" -> pricePerUnit(20),
    "D" -> pricePerUnit(15))

  val cartContents = Random.shuffle(List("A", "A", "A", "A", "B", "B", "B", "C", "D"))

  println("The price of " + cartContents + " = " + price(shopItems)(cartContents).getOrElse("unexpected item in the bagging area"))
}