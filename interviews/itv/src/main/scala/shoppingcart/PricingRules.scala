package shoppingcart

object PricingRules {
  def pricePerUnit(perUnit: Int)(amount: Int) = perUnit * amount
  def specialPriceForM(m: Int, dealPrice: Int, remainderPrice: Int)(amount: Int) =
    ((amount / m) * dealPrice) + ((amount % m) * remainderPrice)

}
