package shoppingcart

import scalaz._
import Scalaz._

object ShoppingCart {
  type Sku = String
  type ErrorMessagesOr[A] = ValidationNel[String, A]
  type PricingRule = Int => Int

  def price(deals: Map[Sku, PricingRule])(items: Seq[Sku]): ErrorMessagesOr[Int] = {
    def validateItem(item: (Sku, Int)) = deals.get(item._1).fold[ValidationNel[String, (Sku, Int)]](s"No price for SKU ${item._1}".failureNel)(Function.const(item.successNel))

    val validatedItems = items.groupBy(identity).mapValues(_.size).map(validateItem).toList
    val pricesOrErrors = validatedItems.sequence[ErrorMessagesOr, (Sku, Int)]
    pricesOrErrors.map(_.map { case (sku, amount) => deals(sku)(amount)}.sum)
  }
}
